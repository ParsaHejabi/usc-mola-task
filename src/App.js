import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import InputGroup from 'react-bootstrap/InputGroup';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import axios from 'axios';

const App = () => {
  const [usernameFieldValue, setUsernameFieldValue] = useState('');
  const [error, setError] = useState('');
  const [validated, setValidated] = useState(false);

  const checkUserAPI = '/api/check-user/';
  const getUserTweetsAPI = '/api/get-tweets/';

  const handleSubmit = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (!usernameFieldValue && usernameFieldValue === '') {
      setError('Please choose a username.');
    } else {
      setValidated(true);
      let userId = await axios.get(`${checkUserAPI}${usernameFieldValue}`);
      if (userId.data.errors) {
        setError(userId.data.errors[0].detail);
      } else if (userId.data.data) {
        userId = userId.data.data.id;
        let tweets = await axios.get(`${getUserTweetsAPI}${userId}`);

        if (tweets.data.errors) {
          setError(tweets.data.errors[0].detail);
        } else {
          setError('');
          tweets = tweets.data.data;

          const fileName = `tweets_${usernameFieldValue}_${userId}`;
          const blob = new Blob([JSON.stringify(tweets, null, 2)], {
            type: 'application/json',
          });
          const href = await URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = href;
          link.download = fileName + '.json';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          setUsernameFieldValue('');
          alert('10 most recent tweets downloaded for the given user!');
        }
      }
    }
  };

  const onChange = (event) => {
    if (event.target.value === '') {
      setError('Please choose a username.');
    } else {
      setError('');
    }
    setUsernameFieldValue(event.target.value);
  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row>
        <Form.Group
          as={Col}
          md="10"
          sm="10"
          xs="10"
          controlId="validationCustomUsername"
        >
          <Form.Label>Enter Twitter username:</Form.Label>
          <InputGroup hasValidation>
            <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
            <Form.Control
              value={usernameFieldValue}
              type="text"
              placeholder="Twitter username"
              aria-describedby="inputGroupPrepend"
              required
              onChange={onChange}
              isInvalid={error !== ''}
            />
            <Form.Control.Feedback type="invalid">
              {error}
            </Form.Control.Feedback>
          </InputGroup>
        </Form.Group>
      </Row>
      <Row>
        <Col xs="auto" className="my-1">
          <Button type="submit">Download tweets</Button>
        </Col>
      </Row>
    </Form>
  );
};

export default App;
