const express = require('express');
const axios = require('axios').default;
const path = require('path');

require('dotenv').config();

const app = express();
let port = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const checkUserAPI = new URL('https://api.twitter.com/2/users/by/username/');
const getUserTweetsAPI = new URL('https://api.twitter.com/2/users/');

const authMessage = {
  title: 'Could not authenticate',
  details: [`Please make sure your bearer token is correct.`],
  type: 'https://developer.twitter.com/en/docs/authentication',
};

app.get('/api/check-user/:username', (req, res) => {
  if (!process.env.TWITTER_BEARER_TOKEN) {
    res.status(400).send(authMessage);
  }

  const token = process.env.TWITTER_BEARER_TOKEN;
  axios
    .get(`${checkUserAPI}${req.params.username}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    // if response.data had errors array in it then say we don't have such user
    // if it has data object in it we have a user
    .then((response) => {
      if (response.status !== 200) {
        if (response.status === 403) {
          res.status(403).send(response.data);
        } else {
          throw new Error(response.data);
        }
      } else {
        res.send(response.data);
      }
    })
    .catch((error) => {
      throw new error(error);
    });
});

app.get('/api/get-tweets/:userId', (req, res) => {
  if (!process.env.TWITTER_BEARER_TOKEN) {
    res.status(400).send(authMessage);
  }

  const token = process.env.TWITTER_BEARER_TOKEN;
  // my twitter id is 574449389
  axios
    .get(`${getUserTweetsAPI}${req.params.userId}/tweets`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    // if response.data had errors array in it then say we don't have such user
    // if it has data object in it we have a user
    .then((response) => {
      if (response.status !== 200) {
        if (response.status === 403) {
          res.status(403).send(response.data);
        } else {
          throw new Error(response.data);
        }
      } else {
        res.send(response.data);
      }
    })
    .catch((error) => {
      throw new Error(error);
    });
});

console.log('NODE_ENV is', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '../build')));
  app.get('*', (request, res) => {
    res.sendFile(path.join(__dirname, '../build', 'index.html'));
  });
} else {
  port = 3001;
}

app.listen(port, () => console.log(`Listening on port ${port}`));
